<?php

namespace App;

use App\Models\ItemModifier;

final class GildedRose
{

    private $items = [];

    /**
     * GildedRose constructor.
     * @param Item[] $items
     */
    public function __construct($items)
    {
        $this->items = $items;
    }

    /**
     * Feels that getter is needed.
     * Returns item array, provided in object creation
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    public function updateQuality()
    {
        $itemModifier = new ItemModifier();
        foreach ($this->items as $item) {
            /**
             * Would be nicer to have custom logic embedded in Item class.
             * But it can not be extended/overridden, so we need to use wrapper.
             */
            $itemModifier->updateQuality($item);
        }
    }
}
