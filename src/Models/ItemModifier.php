<?php

namespace App\Models;

use App\Item;

class ItemModifier
{
    const ITEM_TYPE_DEFAULT = 0;
    const ITEM_TYPE_AGING = 1;
    const ITEM_TYPE_TICKET = 2;
    const ITEM_TYPE_LEGENDARY = 3;
    const ITEM_TYPE_CONJURED = 4;

    /**
     * Primitive type association with name, as by default it is the only way to know what type the item is.
     * @param Item $item
     * @return int
     */
    public function getItemType(Item $item)
    {
        switch ($item->name) {
            default:
                return self::ITEM_TYPE_DEFAULT;

            case 'Sulfuras, Hand of Ragnaros':
                return self::ITEM_TYPE_LEGENDARY;

            case 'Backstage passes to a TAFKAL80ETC concert':
                return self::ITEM_TYPE_TICKET;

            case 'Aged Brie':
                return self::ITEM_TYPE_AGING;

            case 'Conjured Mana Cake':
                return self::ITEM_TYPE_CONJURED;
        }
    }

    /**
     * @param Item $item
     */
    public function updateQuality(Item $item)
    {
        $type = $this->getItemType($item);
        $this->modifySellIn($item, $type);
        $this->modifyQuality($item, $type);
    }

    /**
     * @param Item $item
     * @param int $type
     */
    protected function modifySellIn(Item $item, int $type = self::ITEM_TYPE_DEFAULT)
    {
        switch ($type) {
            //Don't change anything on legendary
            case self::ITEM_TYPE_LEGENDARY:
                return;

            default:
                $item->sell_in--;
        }
    }

    /**
     * @param Item $item
     * @param int $type
     */
    protected function modifyQuality(Item $item, int $type = self::ITEM_TYPE_DEFAULT)
    {
        $quality = $item->quality;
        $change = 1;
        if ($item->sell_in < 0) {
            $change = 2;
        }
        switch ($type) {
            //Don't change anything on legendary
            case self::ITEM_TYPE_LEGENDARY:
                if ($item->quality != 80) { //lets fix legendary item quality
                    $item->quality = 80;
                }
                return;

            case self::ITEM_TYPE_AGING:
                $quality += $change;
                break;

            case self::ITEM_TYPE_TICKET:
                if ($item->sell_in >= 10) {
                    $quality++;
                } elseif ($item->sell_in >= 5) {
                    $quality += 2;
                } elseif ($item->sell_in >= 0) {
                    $quality += 3;
                } else {
                    $quality = 0;
                }
                break;
            case self::ITEM_TYPE_CONJURED:
                $quality -= 2 * $change;
                break;

            default:
                $quality -= $change;
        }
        if ($quality > 50) {
            $quality = 50;
        } elseif ($quality < 0) {
            $quality = 0;
        }
        $item->quality = $quality;
    }
}
