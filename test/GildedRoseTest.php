<?php

namespace App;

use App\Models\ItemModifier;
use PHPUnit\Framework\TestCase;

class GildedRoseTest extends TestCase
{
    /**
     * Test count of days, to find all possible changes
     * Might need to be changed to accommodate input data.
     * The bigger, the slower test will be, as original code, has only one day change cycle
     * @var int
     */
    protected $daysToTest = 10;

    /**
     * Tests if item type provided by data provider, matches the one detected by the code
     * @dataProvider itemDataProvider
     * @param Item $item
     * @param int $type
     */
    public function testQualityType(Item $item, int $type)
    {
//        echo 'Testing type: ' . $item . PHP_EOL;
        /**
         * As we do not know item type, but they have different properties by type,
         * we should make sure, that type is always detected correctly
         */
        $itemModifier = new ItemModifier();
        $this->assertEquals(
            $type,
            $itemModifier->getItemType($item),
            'Quality Type comparison failed, unknown item: ' . $item
        );
    }

    /**
     * Main test of quality changes.
     * Runs changes daily for number of days, set in "daysToTest" parameter
     * And checks changes in returned item, with original provided by data provider
     * @dataProvider itemDataProvider
     * @param Item $item
     * @param int $type
     */
    public function testCheckQualityUpdate(Item $item, int $type)
    {
        /**
         * Save original item, for comparison testing
         */
        $oItem = clone $item;
//        echo 'Testing aging: ' . $item . PHP_EOL;
        $gildedRose = new GildedRose([$item]);
        /**
         * Move days, by setting
         */
        for ($i = 1; $i <= $this->daysToTest; $i++) {
            $gildedRose->updateQuality();
            $this->assertEquals(
                1,
                count($gildedRose->getItems()),
                'If GildedRose class is provided with one item, should return same count'
            );
            //only one item, so take first, test above should make sure it is here
            $mItem = $gildedRose->getItems()[0];
            //make sure, object class did not change after Update
            $this->assertInstanceOf(
                '\App\Item',
                $mItem,
                'GildedRose should return array of \App\Item objects'
            );

            $this->checkBasicInfo($mItem, $oItem, $type);
            $this->checkSellIn($mItem, $oItem, $type, $i);
            $this->checkItemQuality($mItem, $oItem, $type, $i);
        }
    }

    /**
     * Checks basic item info, that would be in predefined constrains
     * @param Item $item
     * @param Item $originalItem
     * @param int $type
     */
    protected function checkBasicInfo(Item $item, Item $originalItem, int $type)
    {
        /**
         * Name should never change, it is not part of quality update daily
         */
        $this->assertEquals(
            $originalItem->name,
            $item->name,
            'Name changed in item from: ' . $originalItem->name . ' to ' . $item->name
        );
        /**
         * Item quality should never be below 0.
         */
        $this->assertGreaterThanOrEqual(
            0,
            $item->quality,
            'Item quality should not be negative: ' . $item
        );
        /**
         * Item quality should never be above 50,
         * except for legendary items, they get 80 by default, and should retain that.
         */
        if ($type !== ItemModifier::ITEM_TYPE_LEGENDARY) {
            $this->assertLessThanOrEqual(
                50,
                $item->quality,
                'Item quality should not be above 50: ' . $item
            );
        } else {
            //Legendary item
            $this->assertEquals(
                80,
                $item->quality,
                'Legendary items should have item quality at 80: ' . $item
            );
        }
    }

    /**
     * Checks sell_in change. It should change by one, daily,
     * except for legendaries, where they do not change at all
     * @param Item $item
     * @param Item $originalItem
     * @param int $type
     * @param int $day
     */
    protected function checkSellIn(Item $item, Item $originalItem, int $type, int $day)
    {
        /**
         * Precalculated sell_in difference.
         */
        $sellInDiff = $originalItem->sell_in - $item->sell_in;
        //check sell_in decay
        switch ($type) {
            case ItemModifier::ITEM_TYPE_LEGENDARY:
                /**
                 * Legendary items do not change the sell_in value
                 */
                $this->assertEquals(
                    0,
                    $sellInDiff,
                    'Legendary items should not decay the "sell_in" value: ' . $item
                );
                break;

            default:
                /**
                 * All items change sell_in by one per day
                 */
                $this->assertEquals(
                    $day,
                    $sellInDiff,
                    'Item sell_in should decay daily: ' . $item
                );
        }
    }


    /**
     * Checks quality change in item
     *
     * Original description:
     * - All items have a SellIn value which denotes the number of days we have to sell the item
     * - All items have a Quality value which denotes how valuable the item is
     * - At the end of each day our system lowers both values for every item
     *
     * - Once the sell by date has passed, Quality degrades twice as fast
     * - The Quality of an item is never negative
     * - "Aged Brie" actually increases in Quality the older it gets
     * - The Quality of an item is never more than 50
     * - "Sulfuras", being a legendary item, never has to be sold or decreases in Quality
     * - "Backstage passes", like aged brie, increases in Quality as its SellIn value approaches;
     * Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but
     * Quality drops to 0 after the concert
     * - "Conjured" items degrade in Quality twice as fast as normal items
     * Just for clarification, an item can never have its Quality increase above 50, however "Sulfuras" is a
     * legendary item and as such its Quality is 80 and it never alters.
     *
     * @param Item $item
     * @param Item $originalItem
     * @param int $type
     * @param int $day
     */
    protected function checkItemQuality(Item $item, Item $originalItem, int $type, int $day): void
    {
        /**
         * Precalculated quality difference
         */
        $qualityDiff = $originalItem->quality - $item->quality;
        /**
         * Default quality changes by one daily, and then twice after sell_in is negative
         * We double absolute value of negative days
         * and add positive days from original, to get that number (if sell_in is negative)
         */
        $qualityDropExpected = $item->sell_in < 0 ? abs($item->sell_in) * 2 + $originalItem->sell_in : $day;
        switch ($type) {
            case ItemModifier::ITEM_TYPE_LEGENDARY:
                //Legendary items do not change quality
                if ($item->quality != 80) {
                    $this->assertEquals(0, $qualityDiff, 'Legendary items should not decay the "quality": ' . $item);
                }
                break;


            case ItemModifier::ITEM_TYPE_AGING:
                //Aging items, have positive quality change, but never above 50
                $expectedQuality = $originalItem->quality + $qualityDropExpected;
                if ($expectedQuality > 50) {
                    $expectedQuality = 50;
                }
                $this->assertEquals(
                    $expectedQuality,
                    $item->quality,
                    'Aging items should increase in quality daily:' . $item,
                );
                break;

            case ItemModifier::ITEM_TYPE_TICKET:
                /**
                 * Tickets have advanced quality control, with zero if sell_in negative
                 * If sell_in positive, we use reverse recount, to find right change.
                 * If sell_in is negative, we know, that quality is zero
                 */
                if ($item->sell_in >= 0) {
                    $expectedQuality = $originalItem->quality;
                    $sellInDay = $item->sell_in;
                    /**
                     *  Reverse recounting after sell_in, to find right change.
                     *  Not optimal, but understandable.
                     */
                    for ($i = 0; $i < $day; $i++) {
                        $sellInDay++;
                        if ($sellInDay > 10) {
                            $expectedQuality++;
                        } elseif ($sellInDay > 5) {
                            $expectedQuality += 2;
                        } else {
                            $expectedQuality += 3;
                        }
                    }
                } else {
                    $expectedQuality = 0;
                }
                if ($expectedQuality > 50) {
                    $expectedQuality = 50;
                }

                $this->assertEquals(
                    $expectedQuality,
                    $item->quality,
                    'Ticket items should increase in quality and be worthless after sell_in is passed:' . $item
                );
                break;

            case ItemModifier::ITEM_TYPE_CONJURED:
                //Conjured items, loose double quality
                $expectedQuality = $originalItem->quality - 2 * $qualityDropExpected;
                if ($expectedQuality < 0) {
                    $expectedQuality = 0;
                }
                $this->assertEquals(
                    $expectedQuality,
                    $item->quality,
                    'Conjured Items should loose double quality daily:' . $item
                );
                break;

            default:
                //Normal items have negative item quality change, but never below zero
                $expectedQuality = $originalItem->quality - $qualityDropExpected;
                if ($expectedQuality < 0) {
                    $expectedQuality = 0;
                }
                $this->assertEquals(
                    $expectedQuality,
                    $item->quality,
                    'Items should loose in quality daily:' . $item
                );
        }
    }

    /**
     * Copy from fixtures/texttest_fixture.php
     * Added types, to check if type detection code is working right
     * @return array[]
     */
    public function itemDataProvider()
    {
        return [
            [new Item('+5 Dexterity Vest', 10, 20), ItemModifier::ITEM_TYPE_DEFAULT],
            [new Item('Aged Brie', 2, 0), ItemModifier::ITEM_TYPE_AGING],
            [new Item('Elixir of the Mongoose', 5, 7), ItemModifier::ITEM_TYPE_DEFAULT],
            [new Item('Sulfuras, Hand of Ragnaros', 0, 70), ItemModifier::ITEM_TYPE_LEGENDARY],
            [new Item('Sulfuras, Hand of Ragnaros', -1, 80), ItemModifier::ITEM_TYPE_LEGENDARY],
            [new Item('Backstage passes to a TAFKAL80ETC concert', 15, 20), ItemModifier::ITEM_TYPE_TICKET],
            [new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49), ItemModifier::ITEM_TYPE_TICKET],
            [new Item('Backstage passes to a TAFKAL80ETC concert', 5, 49), ItemModifier::ITEM_TYPE_TICKET],
            [new Item('Conjured Mana Cake', 3, 6), ItemModifier::ITEM_TYPE_CONJURED],
        ];
    }
}
